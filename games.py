import random
import discord
from discord.ext import commands

class Games():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(description="Roll a dice, from a d4 to d20!")
    async def roll(self, dice='d6'):
        """Roll a dice, from a d4 to d20!"""
        dice_list = ['d4', 'd6', 'd8', 'd10', 'd100', 'd12', 'd20']

        if dice not in dice_list:
            return await self.bot.say('Please roll one of the following dice: ' + ' '.join(dice_list))

        max_roll = int(dice.strip('d'))
        increment = 10 if dice == 'd100' else 1

        await self.bot.say('Rolling ' + str(dice))
        rand_roll = random.randrange(start=(increment * 1), stop=(max_roll + increment), step=increment)
        return await self.bot.say(str(rand_roll))



def setup(bot):
    bot.add_cog(Games(bot))