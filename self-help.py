import time
import variables
import random
import discord
from discord.ext import commands

class SelfHelp():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, description="For when you feel like a deflated little balloon")
    async def delicate(self, msg):
        """For when you feel like a deflated little balloon."""
        await self.bot.say("Don't worry " + str(msg.message.author.name) + ". " + random.choice(variables.delicate))

    @commands.command(description="Top notch jokes for your eyes to read")
    async def joke(self):
        """Top notch jokes for your eyes to read."""
        await self.bot.say(random.choice(variables.jokes))


def setup(bot):
    bot.add_cog(SelfHelp(bot))
