from discord.ext.commands import Bot

add_cogs = ["celebs", "self-help", "violent", "games"]

description="A fun chat-bot written by Stu Franks (stuts)"

asuka = Bot(command_prefix="Asuka ", description=description)

@asuka.event
async def on_read():
    print("Client logged in")

@asuka.command()
async def load(extension_name : str):
    """Loads an extension."""
    try:
        asuka.load_extension(extension_name)
    except (AttributeError, ImportError) as e:
        await asuka.say("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await asuka.say("{} loaded.".format(extension_name))

if __name__ == "__main__":
    for extension in add_cogs:
        try:
            asuka.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension {}\n{}'.format(extension, exc))

    asuka.run("MzI4ODQxOTQwMjAyMjkxMjAw.DDJxcw.1adh57CIDo-z7nFG6TTzc6qNIM8")
