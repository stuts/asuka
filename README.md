
Commands (Currently Implemented)
--------------------------------

Celebs:

* `hey` - Someone's been working hard for you
* `ian` - A Father to some
* `hello` - A friendly hello from a not-so-friendly face

Games:

* `roll` - Roll a dice between d4 and d20 (default: d6)

Self-Help:

* `delicate` - For when you need a little pick-me-up
* `joke` - Just a lil funny for your day

Violent:

* `fight player1 player2` - See how a fight goes between 2 players
* `full_fight player1 player2` - A random blow-by-blow fight
* `troll user` - Send a private message to a user in the channel 


Commands (to be implemented)
----------------------------

* ready - Jeremy Corbyn is back and ready for it all over again
* pout - Send a pouty Asuka face
* wrestling moves - TK, RKO
* penetrate

Features/Things
---------------

* Command timeouts/blocking (only allowed to execute command X every Y seconds)
  * Create command class?
* Local logging

Credits
-------

Asuka Image Source: https://s-media-cache-ak0.pinimg.com/736x/e8/aa/07/e8aa078c77e8bcf867263d2af2b94244.jpg
