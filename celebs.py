import time
import random
import variables
import discord
from discord.ext import commands

class Celebs():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, description='When you feel like no-one is working hard for you')
    async def hey(self, msg):
        """For when you feel like no-one is working hard for you."""
        await self.bot.say("**HEY!** Don't you know Greg Knight's been working hard for you")
        time.sleep(3)
        await self.bot.say("**HEY!** He's been working hard for East Yorkshire too")
        time.sleep(3)
        em = discord.Embed(title="The Honourable Greg Knight")
        em.set_image(url=random.choice(variables.greg_knight))
        await self.bot.send_message(msg.message.channel, embed=em)

    @commands.command(description="Everything else is irrelevant")
    async def ian(self):
        """Everything else is irrelevant."""
        await self.bot.say("<:ian:328530374210617344> " + str(random.choice(variables.ian)))

    @commands.command(pass_context=True, description="A friendly hello from a not-so-friendly face")
    async def hello(self, msg):
        """A friendly hello from a not-so-friendly face."""
        em = discord.Embed(title="***HELLO***")
        em.set_image(url=random.choice(variables.bjorn))
        await self.bot.send_message(msg.message.channel, embed=em)


def setup(bot):
    bot.add_cog(Celebs(bot))
