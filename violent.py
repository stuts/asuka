import variables
import random
import time
import discord
from discord.ext import commands
from statistics import mode


class Violent():
    def __init__(self, bot):
        self.bot = bot

    @staticmethod
    def _fight_check(winner_list, players):
        players = list(players)

        if len(players) >= 3 or len(players) < 2:
            return False

        winner = random.choice(players)
        winner_list.append(winner)
        players.remove(winner)

        return winner_list, winner + ' ' + random.choice(variables.attacks) + ' ' + players[0]

    @commands.command(description="Let's settle this the old fashioned way")
    async def fight(self, *players):
        """Start a fight between 2 people."""
        players = list(players)
        winner_list = []
        winner_list, result = self._fight_check(winner_list, players)

        if not result:
            return await self.bot.say('Please only enter 2 players')
        else:
            return await self.bot.say(result)

    @commands.command(description="Let's settle this the old fashioned way")
    async def full_fight(self, *players):
        """Blow-by-blow commentary of a fight."""
        players = list(players)
        winner_list = []
        winner_list, result = self._fight_check(winner_list, players)

        if not result:
            return await self.bot.say('Please only enter 2 players')
        else:
            await self.bot.say(players[0] + " vs " + players[1] + " - FIGHT!")

            await self.bot.say(result)

            time.sleep(2)
            round_count = random.randrange(1, 20)

            for x in list(range(0, round_count)):
                winner_list, result = self._fight_check(winner_list, players)

                if not result:
                    return await self.bot.say('Please only enter 2 players')
                else:
                    await self.bot.say(result)

                time.sleep(2)

            try:
                mode(winner_list)
            except Exception as e:
                return await self.bot.say("The fight is over, it's a draw!")

            return await self.bot.say("The fight is over, the winner is " + mode(winner_list) + "!")

    @commands.command(pass_context=True, description="If someone is annoying you or you feel like being an arsehole")
    async def troll(self, msg, user=None):
        """If someone is annoying you or you feel like being an arsehole."""
        members = msg.message.server.members

        if not user:
            return await self.bot.say('Please follow the command with a user to troll')

        await self.bot.delete_message(msg.message)

        if user not in [member.name for member in list(members)]:
            return await self.bot.say('User does not exist in channel')
        else:
            userobj = next(member for member in list(members) if member.name == user)
            await self.bot.send_message(userobj, "you're a cunt - love from your secret troll")


def setup(bot):
    bot.add_cog(Violent(bot))
